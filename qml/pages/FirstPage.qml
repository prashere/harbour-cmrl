/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id: page
    property int counter: 0
    property bool backStepping: true
    property int operationType: PageStackAction.Animated

    SilicaListView {
        id: stations_view
        anchors.fill: parent
        spacing: Theme.paddingMedium
        property int difference: 0

        header: PageHeader {
            title: "Chennai Metro Rail"
            description: "http://www.chennaimetrorail.gov.in/"
        }

        function getDetails(){
            if (stop_info.visible == false) {
                stop_info.visible = true
            }

            if (stations_view.difference == 0) {
                stop_info.label = "Have You Gone Nuts! You are already here"
                ticket_fare.label = ""
                if (ticket_fare.visible == true) {
                    ticket_fare.visible = false
                }
            }
            else if (stations_view.difference == 1){
                stop_info.label = to.value + " is next stop"
                ticket_fare.label = "Rs. 10/-"
            }
            else if (stations_view.difference == 2){
                stop_info.label = to.value + " is 2nd stop from " + from.value
                ticket_fare.label = "Rs. 20/-"
            }
            else if (stations_view.difference == 3){
                stop_info.label = to.value + " is 3rd stop from " + from.value
                ticket_fare.label = "Rs. 30/-"
            }
            else if (stations_view.difference >= 4){
                stop_info.label = to.value + " is " + stations_view.difference + "th stop from " + from.value
                ticket_fare.label = "Rs. 40/-"
            }

            if (ticket_fare.visible == false && ticket_fare.label != ""){
                ticket_fare.visible = true
            }

        }

        model: VisualItemModel {

            Image {
                id: cmrl_image
                source: "cmrl.jpg"
                width: page.width
            }

            // set currentIndex to change the selected value
            ComboBox {
                id: from
                width: page.width
                label: "From"
                currentIndex: 0
                menu: StationsModel {}
            }

            ComboBox {
                id: to
                width: page.width
                label: "To"
                currentIndex: 0
                menu: StationsModel {}

                onCurrentIndexChanged: {
                    if (page.counter == 0) {
                        if (hint._stopped) {
                            hint.direction = TouchInteraction.Down
                            hint.start()
                            hint_label.visible = true
                        }
                    }
                    page.counter = page.counter + 1
                }
            }

            TextField {
                id: stop_info
                width: page.width
                readOnly: true
                label: ""
                text: "Station Info"
                textMargin: Theme.horizontalPageMargin
                visible: false
            }

            TextField {
                id: ticket_fare
                width: page.width
                readOnly: true
                label: ""
                text: "Ticket Fare"
                visible: false
            }            
        }

        PullDownMenu {

            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("SecondPage.qml"))
            }

            MenuItem {
                text: "Find Info"
                onClicked: {
                    if (hint._stopped != true && hint_label.visible == true) {
                        hint.stop()
                        hint_label.visible = false
                    }

                    if (from.currentIndex == to.currentIndex) {
                        stations_view.difference = 0
                    }
                    else if (from.currentIndex < to.currentIndex){
                        stations_view.difference = to.currentIndex - from.currentIndex
                    }
                    else if (from.currentIndex > to.currentIndex){
                        stations_view.difference = from.currentIndex - to.currentIndex
                    }
                    stations_view.getDetails()
                }
            }
        }
    }

    InteractionHintLabel {
        id: hint_label
        anchors.bottom: parent.bottom
        Behavior { FadeAnimation {} }
        text: "Pull down to find result"
        visible: false
    }

    TouchInteractionHint {
        id: hint
        loops: Animation.Infinite
        interactionMode: TouchInteraction.Pull
    }
}


